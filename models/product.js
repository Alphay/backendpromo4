'use strict';
module.exports = (sequelize, DataTypes) => {

  const Product = sequelize.define('Product', {
    id : {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    category: DataTypes.STRING,
    price: DataTypes.INTEGER,
    userName: DataTypes.STRING,
    email: DataTypes.STRING,
    description: DataTypes.STRING,
    phoneNumber: DataTypes.INTEGER,
    images: DataTypes.TEXT
  }, {});
  Product.associate = function(models) {
    /** associations are defined here */ 

    /**ForeignKey id_Product in table ToBuy*/
    Product.belongsToMany(models.ShoppingCart, {as: 'tobuy', through: models.ToBuy, foreignKey:'idProduct', otherKey:'id', onDelete: 'CASCADE'})

  };
  return Product;
};