'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
   return Promise.all([
    queryInterface.addColumn(
      'Products',
      'phoneNumber',
      {
        type: Sequelize.STRING
      }
    ),
    queryInterface.addColumn(
      'Products',
      'description',
      {
        type: Sequelize.STRING
      }
    ),
  ]);
},

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        'Products',
        'phoneNumber'
      ),
      queryInterface.removeColumn(
        'Products',
        'description'
      ),
    ]);
  },
  
};
