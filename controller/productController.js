const models = require('../models');

/**
 * Controller responsible of the Product
 */
class ProductController {
    /**Method to return all products*/
    static getAll(req, res) {
        models.Product.findAll().then(Products => {
            res.send(Products)
        })
    }

    /**Method to get a product by its id*/
    static getById(req, res) {
        models.Product.findAll().then(Products => {
            res.send(Products)
        })
    }

    static create(req, res) {
        res.send('is ok')
    }

    static update(req, res) {
        res.send('is ok')
    }

    static delete(req, res) {
        res.send('is ok')
    }

    static show(req, res) {
        models.Product.findByPk(req.params.id).then(Products => {
            res.send(Products)
        })
    }

    /**
     *  register a product
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     */
    static register(req, res) {

        // input
        const name = req.body.name;
        const category = req.body.category;
        const email = req.body.email
        const phoneNumber = req.body.phoneNumber
        const description = req.body.description
        const price = req.body.price
        const images = req.body.images
        const userName = req.body.userName;
        console.log(price,req.body.price)
        models.Product.create({
            name: name,
            category: category,
            price: price,
            phoneNumber: phoneNumber,
            description: description,
            images: images,
            email: email,
            userName: userName
        }).then(newProduct => {
                res.send({ message: "Product created", product: newProduct })

            })
            // if there was an error creating the product
            .catch((error) => {
                res.status(500).json({ error: 'cannot add product',message:error })
            })
        // verify if the user already exists
        
    }

}

module.exports = ProductController