'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
   return Promise.all([
    queryInterface.addColumn(
      'Products',
      'userName',
      {
        type: Sequelize.STRING
      }
    ),
    queryInterface.addColumn(
      'Products',
      'email',
      {
        type: Sequelize.STRING
      }
    ),
  ]);
},

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        'Products',
        'userName'
      ),
      queryInterface.removeColumn(
        'Products',
        'email'
        
      ),
    ]);
  },
};
